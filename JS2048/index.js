OnLoad();

function OnLoad() {
    try {
        //初始化16个格子对象
        for (let index = 0; index < 16; index++) {
            var box = new Box(index);
            boxs.push(box);
        }
        Initialize();
    } catch (error) {
        alert(error);
        Initialize();
    }
}

//初始化
function Initialize() {
    try {
        arrY = new Array();
        //初始化4*4数组
        var boxNum = 0;
        for (let y = 0; y < 4; y++) {
            var arrX = new Array();
            for (let x = 0; x < 4; x++) {
                boxs[boxNum].ChangeText('');
                arrX.push(boxs[boxNum]);
                boxNum = boxNum + 1;
            }
            arrY.push(arrX);
        }

        RanBox();

    } catch (error) {
        throw error;
    }
}

//监听键盘
document.onkeydown = function (event) {
    try {
        var e = event || window.event || arguments.callee.caller.arguments[0];

        switch (e.keyCode) {
            case 37:
                //alert('Left');

                for (let y = 0; y < 4; y++) {
                    for (let x = 0; x < 3; x++) {
                        if (arrY[y][x].BoxValue == '') {
                            //当前格子为空，向右找到第一个不为空的放到当前格子
                            for (let index = x + 1; index < 4; index++) {
                                if (arrY[y][index].BoxValue != '') {
                                    arrY[y][x].ChangeText(arrY[y][index].BoxValue);
                                    arrY[y][index].ChangeText('');
                                    break;
                                }
                            }
                        } else {
                            //当前格子不为空，向右找到第一个不为空的与当前格子比较
                            for (let index = x + 1; index < 4; index++) {
                                if (arrY[y][index].BoxValue != '') {
                                    if (arrY[y][x].BoxValue != arrY[y][index].BoxValue) {
                                        var num = arrY[y][index].BoxValue;
                                        arrY[y][index].ChangeText('');
                                        arrY[y][x + 1].ChangeText(num);
                                    } else {
                                        arrY[y][x].ChangeText(arrY[y][x].BoxValue * 2);
                                        arrY[y][index].ChangeText('');
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                KeyDownAfter();
                break;
            case 38:
                //alert('Up');

                for (let x = 0; x < 4; x++) {
                    for (let y = 0; y < 3; y++) {
                        if (arrY[y][x].BoxValue == '') {
                            //当前格子为空，向下找到第一个不为空的放到当前格子
                            for (let index = y + 1; index < 4; index++) {
                                if (arrY[index][x].BoxValue != '') {
                                    arrY[y][x].ChangeText(arrY[index][x].BoxValue);
                                    arrY[index][x].ChangeText('');
                                    break;
                                }
                            }
                        } else {
                            //当前格子不为空，向下找到第一个不为空的与当前格子比较
                            for (let index = y + 1; index < 4; index++) {
                                if (arrY[index][x].BoxValue != '') {
                                    if (arrY[y][x].BoxValue != arrY[index][x].BoxValue) {
                                        var num = arrY[index][x].BoxValue;
                                        arrY[index][x].ChangeText('');
                                        arrY[y+1][x].ChangeText(num);
                                    } else {
                                        arrY[y][x].ChangeText(arrY[y][x].BoxValue * 2);
                                        arrY[index][x].ChangeText('');
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                
                KeyDownAfter();
                break;
            case 39:
                //alert('Right');

                for (let y = 0; y < 4; y++) {
                    for (let x = 3; x > 0; x--) {
                        if (arrY[y][x].BoxValue == '') {
                            //当前格子为空，向左找到第一个不为空的放到当前格子
                            for (let index = x - 1; index >=0; index--) {
                                if (arrY[y][index].BoxValue != '') {
                                    arrY[y][x].ChangeText(arrY[y][index].BoxValue);
                                    arrY[y][index].ChangeText('');
                                    break;
                                }
                            }
                        } else {
                            //当前格子不为空，向左找到第一个不为空的与当前格子比较
                            for (let index = x - 1; index >=0; index--) {
                                if (arrY[y][index].BoxValue != '') {
                                    if (arrY[y][x].BoxValue != arrY[y][index].BoxValue) {
                                        var num = arrY[y][index].BoxValue;
                                        arrY[y][index].ChangeText('');
                                        arrY[y][x - 1].ChangeText(num);
                                    } else {
                                        arrY[y][x].ChangeText(arrY[y][x].BoxValue * 2);
                                        arrY[y][index].ChangeText('');
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }


                KeyDownAfter();
                break;
            case 40:
                //alert('Down');

                for (let x = 0; x < 4; x++) {
                    for (let y = 3; y > 0; y--) {
                        if (arrY[y][x].BoxValue == '') {
                            //当前格子为空，向上找到第一个不为空的放到当前格子
                            for (let index = y - 1; index >= 0; index--) {
                                if (arrY[index][x].BoxValue != '') {
                                    arrY[y][x].ChangeText(arrY[index][x].BoxValue);
                                    arrY[index][x].ChangeText('');
                                    break;
                                }
                            }
                        } else {
                            //当前格子不为空，向上找到第一个不为空的与当前格子比较
                            for (let index = y - 1; index >= 0; index--) {
                                if (arrY[index][x].BoxValue != '') {
                                    if (arrY[y][x].BoxValue != arrY[index][x].BoxValue) {
                                        var num = arrY[index][x].BoxValue;
                                        arrY[index][x].ChangeText('');
                                        arrY[y-1][x].ChangeText(num);
                                    } else {
                                        arrY[y][x].ChangeText(arrY[y][x].BoxValue * 2);
                                        arrY[index][x].ChangeText('');
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }


                KeyDownAfter();
                break;
            default:
                break;
        }

    } catch (error) {
        alert(error);
        Initialize();
    }
};

